# forum-lqdn

Un rôle basé sur Ansible qui permet l'installation et la configuration de Discourse, comme forum de La Quadrature du Net.

## Installation

Ce rôle doit être utilisé dans un playbook ansible. Voir les exemples pour son utilisation. À priori, il suffit de télécharger ou de cloner ce dépôt dans le dossier `roles/` pour pouvoir l'utiliser.

## Updates

Ce rôle est sans états, la mise à jour peut donc simplement se faire en exécutant de nouveau le rôle. L'image docker sera mise à jour automatiquement, pour un peu que l'option de version soit `latest-relase`.

Les données sont stockés hors de l'image Docker.

## Variables

- Where should the git directory be cloned

  discourse_install_path: "/opt/discourse"

- The home for the discourse user, should stay empty.

  discourse_home_path: "/home/discourse"

- Where the data ( uploads, database, etc.. ) should be stored

  discourse_storage_location: "/var/discourse/data"

- Where the application logs should be stored

  discourse_log_location: "/var/log/discourse"

- The discourse version you want to use. Use `latest-relase` to always be up to date.

  discourse_discourse_version: "v2.8.0"

- The domain name you want to use.

  discourse_domain_name: ""

- Where should the emails be sent

  discourse_admin_email_account: ""

- The first user of your forum credential's

  discourse_forum_admin_user: ""
  discourse_forum_admin_password: ""

- SMTP settings

  discourse_smtp_address: ""
  discourse_smtp_port: ""
  discourse_smtp_username: ""
  discourse_smtp_auth: ""
  discourse_smtp_openssl_verify: ""
  discourse_smtp_password: ""
  discourse_smtp_starttls: ""


## Dépendances

Ce rôle automatise l'installation de Discourse tel que prévu par les mainteneurs du projet, et nécessite donc une machine où l'utilisation de Docker est possible.

Voir : https://github.com/discourse/discourse_docker

Et plus précisement :

- https://github.com/discourse/discourse/blob/main/docs/INSTALL-cloud.md
- https://github.com/discourse/discourse_docker

Documentation :

- https://github.com/discourse/discourse/tree/main/docs

## Exemple

## Licence

AGPLv3.0 - Voir le fichier LICENCE.md

## Crédit

Créer par [nono](mailto:np@laquadrature.net) pour La Quadrature du Net
